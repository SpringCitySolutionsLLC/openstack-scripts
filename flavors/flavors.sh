#!/bin/bash
#
# openstack-scripts/flavors/flavors.sh
#

source /etc/kolla/admin-openrc.sh

openstack flavor delete 01
openstack flavor create --id 01 --vcpus 1 --ram 2048 --disk 16 --public 1-cpu-02G-ram-16G-disk

openstack flavor delete 02
openstack flavor create --id 02 --vcpus 1 --ram 2048 --disk 32 --public 1-cpu-02G-ram-32G-disk

openstack flavor delete 03
openstack flavor create --id 03 --vcpus 1 --ram 4096 --disk 32 --public 1-cpu-04G-ram-32G-disk

openstack flavor delete 04
openstack flavor create --id 04 --vcpus 1 --ram 8192 --disk 32 --public 1-cpu-08G-ram-32G-disk

openstack flavor delete 05
openstack flavor create --id 05 --vcpus 1 --ram 16384 --disk 32 --public 1-cpu-16G-ram-32G-disk

openstack flavor delete 06
openstack flavor create --id 06 --vcpus 2 --ram 8192 --disk 32 --public 2-cpu-08G-ram-32G-disk

openstack flavor delete 07
openstack flavor create --id 07 --vcpus 4 --ram 8192 --disk 32 --public 4-cpu-08G-ram-32G-disk

openstack flavor list

exit 0
