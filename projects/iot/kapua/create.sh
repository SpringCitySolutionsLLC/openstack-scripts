#!/bin/bash
#
# openstack-scripts/projects/iot/kapua/create.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack create \
  -t kapua.yml \
  kapua_stack

exit 0

#
