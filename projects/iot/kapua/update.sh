#!/bin/bash
#
# openstack-scripts/projects/iot/kapua/update.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack update \
  -t kapua.yml \
  kapua_stack

exit 0

#
