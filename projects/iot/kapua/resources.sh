#!/bin/bash
#
# openstack-scripts/projects/iot/kapua/resources.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack resource list \
  -f yaml \
  kapua_stack

exit 0

#
