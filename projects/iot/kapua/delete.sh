#!/bin/bash
#
# openstack-scripts/projects/iot/kapua/delete.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack delete -y kapua_stack

exit 0

#
