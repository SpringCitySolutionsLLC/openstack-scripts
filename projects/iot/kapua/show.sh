#!/bin/bash
#
# openstack-scripts/projects/iot/kapua/show.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack show \
  -f yaml \
  kapua_stack

exit 0

#
