#!/bin/bash
#
# openstack-scripts/projects/iot/iot/create.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack create \
  -t iot.yml \
  iot_stack

exit 0

#
