#!/bin/bash
#
# openstack-scripts/projects/iot/iot/update.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack update \
  -t iot.yml \
  iot_stack

exit 0

#
