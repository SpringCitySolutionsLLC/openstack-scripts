#!/bin/bash
#
# openstack-scripts/projects/iot/iot/delete.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack delete -y iot_stack

exit 0

#
