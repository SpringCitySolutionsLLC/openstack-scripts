#!/bin/bash
#
# openstack-scripts/projects/iot/iot/show.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack show \
  -f yaml \
  iot_stack

exit 0

#
