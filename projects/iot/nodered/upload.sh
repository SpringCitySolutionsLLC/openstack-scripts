#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/upload.sh
#

source /etc/kolla/admin-iot-openrc.sh

chown 1000:1000 protobufs/*

openstack appcontainer cp protobufs/* nodered:/data

zun exec nodered /bin/bash -c 'chown -R node-red:node-red /data/*'

exit 0

#
