#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/create.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack create \
  -t nodered.yml \
  nodered_stack

exit 0

#
