#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/interactive.sh
#

source /etc/kolla/admin-iot-openrc.sh

zun exec --interactive nodered /bin/bash

exit 0

#
