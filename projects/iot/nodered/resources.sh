#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/resources.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack resource list \
  -f yaml \
  nodered_stack

exit 0

#
