#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/delete.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack delete -y nodered_stack

exit 0

#
