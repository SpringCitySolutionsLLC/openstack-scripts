#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/restore.sh
#
# See also, backup.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-iot-openrc.sh

openstack appcontainer cp /root/backup/nodered/flows_cred.json nodered:/data
openstack appcontainer cp /root/backup/nodered/flows.json nodered:/data
openstack appcontainer cp /root/backup/nodered/settings.js nodered:/data

zun exec nodered /bin/bash -c 'chown -R node-red:node-red /data/*'

exit 0

#
