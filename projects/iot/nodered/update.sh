#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/update.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack update \
  -t nodered.yml \
  nodered_stack

exit 0

#
