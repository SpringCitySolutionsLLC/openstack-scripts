#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/show.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack show \
  -f yaml \
  nodered_stack

exit 0

#
