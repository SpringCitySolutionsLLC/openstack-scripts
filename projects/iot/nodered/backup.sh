#!/bin/bash
#
# openstack-scripts/projects/iot/nodered/backup.sh
#
# See also, restore.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-iot-openrc.sh

mkdir -p transfer

openstack appcontainer cp nodered:/data/settings.js transfer
openstack appcontainer cp nodered:/data/flows.json transfer
openstack appcontainer cp nodered:/data/flows_cred.json transfer

rm -Rf /root/backup/nodered

mkdir -p /root/backup/nodered

mv transfer/* /root/backup/nodered

rm -Rf transfer

exit 0

#
