#!/bin/bash
#
# openstack-scripts/projects/iot/kura/create.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack create \
  -t kura.yml \
  kura_stack

exit 0

#
