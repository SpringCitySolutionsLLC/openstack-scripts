#!/bin/bash
#
# openstack-scripts/projects/iot/kura/resources.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack resource list \
  -f yaml \
  kura_stack

exit 0

#
