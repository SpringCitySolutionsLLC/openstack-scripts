#!/bin/bash
#
# openstack-scripts/projects/iot/kura/show.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack show \
  -f yaml \
  kura_stack

exit 0

#
