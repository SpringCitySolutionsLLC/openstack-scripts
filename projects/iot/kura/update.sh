#!/bin/bash
#
# openstack-scripts/projects/iot/kura/update.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack update \
  -t kura.yml \
  kura_stack

exit 0

#
