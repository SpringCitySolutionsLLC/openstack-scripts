#!/bin/bash
#
# openstack-scripts/projects/iot/kura/delete.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack delete -y kura_stack

exit 0

#
