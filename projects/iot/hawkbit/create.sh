#!/bin/bash
#
# openstack-scripts/projects/iot/hawkbit/create.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack create \
  -t hawkbit.yml \
  hawkbit_stack

exit 0

#
