#!/bin/bash
#
# openstack-scripts/projects/iot/hawkbit/resources.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack resource list \
  -f yaml \
  hawkbit_stack

exit 0

#
