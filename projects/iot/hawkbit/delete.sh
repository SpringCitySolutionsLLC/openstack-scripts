#!/bin/bash
#
# openstack-scripts/projects/iot/hawkbit/delete.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack delete -y hawkbit_stack

exit 0

#
