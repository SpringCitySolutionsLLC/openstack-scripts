#!/bin/bash
#
# openstack-scripts/projects/iot/hawkbit/show.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack show \
  -f yaml \
  hawkbit_stack

exit 0

#
