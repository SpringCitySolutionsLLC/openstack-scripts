#!/bin/bash
#
# openstack-scripts/projects/iot/hawkbit/update.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack update \
  -t hawkbit.yml \
  hawkbit_stack

exit 0

#
