#!/bin/bash
#
# openstack-scripts/projects/iot/mqttrouter/create.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack create \
  -t mqttrouter.yml \
  mqttrouter_stack

exit 0

#
