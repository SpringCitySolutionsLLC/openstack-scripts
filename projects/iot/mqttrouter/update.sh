#!/bin/bash
#
# openstack-scripts/projects/iot/mqttrouter/update.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack update \
  -t mqttrouter.yml \
  mqttrouter_stack

exit 0

#
