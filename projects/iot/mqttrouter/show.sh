#!/bin/bash
#
# openstack-scripts/projects/iot/mqttrouter/show.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack show \
  -f yaml \
  mqttrouter_stack

exit 0

#
