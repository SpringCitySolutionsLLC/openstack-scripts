#!/bin/bash
#
# openstack-scripts/projects/iot/mqttrouter/delete.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack delete -y mqttrouter_stack

exit 0

#
