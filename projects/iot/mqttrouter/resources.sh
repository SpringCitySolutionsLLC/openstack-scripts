#!/bin/bash
#
# openstack-scripts/projects/iot/mqttrouter/resources.sh
#

source /etc/kolla/admin-iot-openrc.sh

openstack stack resource list \
  -f yaml \
  mqttrouter_stack

exit 0

#
