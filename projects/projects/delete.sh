#!/bin/bash
#
# openstack-scripts/projects/projects/delete.sh
#

source /etc/kolla/admin-openrc.sh

openstack stack delete -y projects_stack

exit 0

#
