#!/bin/bash
#
# openstack-scripts/projects/projects/update.sh
#

source /etc/kolla/admin-openrc.sh

openstack stack update \
  -t projects.yml \
  projects_stack

exit 0

#
