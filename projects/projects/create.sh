#!/bin/bash
#
# openstack-scripts/projects/projects/create.sh
#

source /etc/kolla/admin-openrc.sh

openstack stack create \
  -t projects.yml \
  projects_stack

echo
echo Remember to add users, including admin, to the sysadmins group
echo

exit 0

#
