#!/bin/bash
#
# openstack-scripts/projects/projects/show.sh
#

source /etc/kolla/admin-openrc.sh

openstack stack show \
  -f yaml \
  projects_stack

exit 0

#
