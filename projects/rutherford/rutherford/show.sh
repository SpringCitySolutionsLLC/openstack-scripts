#!/bin/bash
#
# openstack-scripts/projects/rutherford/rutherford/show.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack show \
  -f yaml \
  rutherford_stack

exit 0

#
