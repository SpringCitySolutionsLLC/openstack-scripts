#!/bin/bash
#
# openstack-scripts/projects/rutherford/rutherford/validate.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack orchestration template validate -t rutherford.yml 

exit 0

#
