#!/bin/bash
#
# openstack-scripts/projects/rutherford/rutherford/update.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack update \
  -t rutherford.yml \
  rutherford_stack

exit 0

#
