#!/bin/bash
#
# openstack-scripts/projects/rutherford/rutherford/create.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack create \
  -t rutherford.yml \
  rutherford_stack

exit 0

#
