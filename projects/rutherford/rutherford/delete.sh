#!/bin/bash
#
# openstack-scripts/projects/rutherford/rutherford/delete.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack delete -y rutherford_stack

exit 0

#
