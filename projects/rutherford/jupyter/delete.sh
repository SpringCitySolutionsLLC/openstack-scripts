#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/delete.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack delete -y jupyter_stack

exit 0

#
