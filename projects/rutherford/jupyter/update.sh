#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/update.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack update \
  -t jupyter.yml \
  jupyter_stack

exit 0

#
