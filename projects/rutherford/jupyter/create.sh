#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/create.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack create \
  -t jupyter.yml \
  jupyter_stack

exit 0

#
