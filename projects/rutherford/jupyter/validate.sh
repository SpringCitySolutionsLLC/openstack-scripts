#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/validate.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack orchestration template validate -t jupyter.yml 

exit 0

#
