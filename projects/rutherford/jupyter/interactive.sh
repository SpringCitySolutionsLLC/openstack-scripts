#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/interactive.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

zun exec --interactive jupyter /bin/bash

exit 0

#
