#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/check.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack check jupyter_stack

exit 0

#
