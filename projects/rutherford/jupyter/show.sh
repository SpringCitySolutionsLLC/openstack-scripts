#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/show.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack show \
  -f yaml \
  jupyter_stack

exit 0

#
