#!/bin/bash
#
# openstack-scripts/projects/rutherford/jupyter/resources.sh
#

source /etc/kolla/admin-rutherford-openrc.sh

openstack stack resource list \
  -f yaml \
  jupyter_stack

exit 0

#
