#!/bin/bash
#
# openstack-scripts/projects/server/es21/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y es21_stack

exit 0

#
