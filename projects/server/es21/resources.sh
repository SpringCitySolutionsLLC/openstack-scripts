#!/bin/bash
#
# openstack-scripts/projects/server/es21/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  es21_stack

exit 0

#
