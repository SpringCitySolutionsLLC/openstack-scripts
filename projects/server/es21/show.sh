#!/bin/bash
#
# openstack-scripts/projects/server/es21/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  es21_stack

exit 0

#
