#!/bin/bash
#
# openstack-scripts/projects/server/es21/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t es21.yml \
  es21_stack

exit 0

#
