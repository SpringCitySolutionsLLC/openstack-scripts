#!/bin/bash
#
# openstack-scripts/projects/server/es21/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t es21.yml \
  es21_stack

exit 0

#
