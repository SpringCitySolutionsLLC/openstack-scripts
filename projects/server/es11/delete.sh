#!/bin/bash
#
# openstack-scripts/projects/server/es11/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y es11_stack

exit 0

#
