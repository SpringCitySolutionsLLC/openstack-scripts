#!/bin/bash
#
# openstack-scripts/projects/server/es11/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  es11_stack

exit 0

#
