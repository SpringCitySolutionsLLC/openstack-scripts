#!/bin/bash
#
# openstack-scripts/projects/server/es11/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t es11.yml \
  es11_stack

exit 0

#
