#!/bin/bash
#
# openstack-scripts/projects/server/es11/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t es11.yml \
  es11_stack

exit 0

#
