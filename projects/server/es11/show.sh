#!/bin/bash
#
# openstack-scripts/projects/server/es11/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  es11_stack

exit 0

#
