#!/bin/bash
#
# openstack-scripts/projects/server/storage1/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  storage1_stack

exit 0

#
