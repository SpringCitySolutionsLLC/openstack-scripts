#!/bin/bash
#
# openstack-scripts/projects/server/storage1/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t storage1.yml \
  storage1_stack

exit 0

#
