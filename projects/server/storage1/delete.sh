#!/bin/bash
#
# openstack-scripts/projects/server/storage1/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y storage1_stack

exit 0

#
