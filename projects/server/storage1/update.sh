#!/bin/bash
#
# openstack-scripts/projects/server/storage1/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t storage1.yml \
  storage1_stack

exit 0

#
