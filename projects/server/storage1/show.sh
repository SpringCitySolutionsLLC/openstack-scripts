#!/bin/bash
#
# openstack-scripts/projects/server/storage1/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  storage1_stack

exit 0

#
