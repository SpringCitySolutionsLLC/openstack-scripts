#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t mattermost.yml \
  mattermost_stack

exit 0

#
