#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/check.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack check mattermost_stack

exit 0

#
