#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  mattermost_stack

exit 0

#
