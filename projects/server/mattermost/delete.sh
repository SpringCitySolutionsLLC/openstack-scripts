#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y mattermost_stack

exit 0

#
