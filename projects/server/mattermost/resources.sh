#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  mattermost_stack

exit 0

#
