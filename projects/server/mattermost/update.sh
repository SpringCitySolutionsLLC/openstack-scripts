#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t mattermost.yml \
  mattermost_stack

exit 0

#
