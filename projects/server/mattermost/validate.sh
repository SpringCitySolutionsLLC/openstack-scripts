#!/bin/bash
#
# openstack-scripts/projects/server/mattermost/validate.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack orchestration template validate -t mattermost.yml 

exit 0

#
