#!/bin/bash
#
# openstack-scripts/projects/server/tasmoadmin/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y tasmoadmin_stack

exit 0

#
