#!/bin/bash
#
# openstack-scripts/projects/server/tasmoadmin/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t tasmoadmin.yml \
  tasmoadmin_stack

exit 0

#
