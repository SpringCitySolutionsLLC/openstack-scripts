#!/bin/bash
#
# openstack-scripts/projects/server/tasmoadmin/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  tasmoadmin_stack

exit 0

#
