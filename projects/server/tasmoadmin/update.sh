#!/bin/bash
#
# openstack-scripts/projects/server/tasmoadmin/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t tasmoadmin.yml \
  tasmoadmin_stack

exit 0

#
