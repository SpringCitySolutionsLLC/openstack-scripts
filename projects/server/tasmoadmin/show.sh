#!/bin/bash
#
# openstack-scripts/projects/server/tasmoadmin/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  tasmoadmin_stack

exit 0

#
