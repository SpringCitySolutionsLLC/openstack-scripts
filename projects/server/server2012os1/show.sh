#!/bin/bash
#
# openstack-scripts/projects/server/server2012os1/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  server2012os1_stack

exit 0

#
