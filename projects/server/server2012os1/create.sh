#!/bin/bash
#
# openstack-scripts/projects/server/server2012os1/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t server2012os1.yml \
  server2012os1_stack

exit 0

#
