#!/bin/bash
#
# openstack-scripts/projects/server/server2012os1/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t server2012os1.yml \
  server2012os1_stack

exit 0

#
