#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  pocketmine_stack

exit 0

#
