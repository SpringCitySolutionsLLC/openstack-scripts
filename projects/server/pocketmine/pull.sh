#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/pull.sh
#

source /etc/kolla/admin-server-openrc.sh

docker pull pmmp/pocketmine-mp

openstack image delete pocketmine

docker save pmmp/pocketmine-mp | \
  openstack image create \
    --container-format docker \
    --disk-format raw \
    --project server \
    pocketmine

exit 0

#
