#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/backup.sh
#
# See also, restore.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-server-openrc.sh

rm -Rf /root/backup/pocketmine

mkdir -p transfer

openstack appcontainer cp pocketmine:/data transfer

mkdir -p /root/backup/pocketmine/data

mv transfer/data /root/backup/pocketmine

rm -Rf transfer

mkdir -p transfer

openstack appcontainer cp pocketmine:/plugins transfer

mkdir -p /root/backup/pocketmine/plugins

mv transfer/plugins /root/backup/pocketmine

rm -Rf transfer

exit 0

#
