#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/restore.sh
#
# See also, backup.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-server-openrc.sh

zun exec pocketmine /bin/bash -c 'rm -Rf /data/*'
openstack appcontainer cp /root/backup/pocketmine/data pocketmine:/
zun exec pocketmine /bin/bash -c 'chown -R pocketmine:pocketmine /data'

zun exec pocketmine /bin/bash -c 'rm -Rf /plugins/*'
openstack appcontainer cp /root/backup/pocketmine/plugins pocketmine:/
zun exec pocketmine /bin/bash -c 'chown -R pocketmine:pocketmine /plugins'

exit 0

#
