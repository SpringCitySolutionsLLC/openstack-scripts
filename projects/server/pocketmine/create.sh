#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t pocketmine.yml \
  pocketmine_stack

exit 0

#
