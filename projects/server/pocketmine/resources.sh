#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  pocketmine_stack

exit 0

#
