#!/bin/bash
#
# openstack-scripts/projects/server/pocketmine/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t pocketmine.yml \
  pocketmine_stack

exit 0

#
