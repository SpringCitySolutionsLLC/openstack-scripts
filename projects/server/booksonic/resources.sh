#!/bin/bash
#
# openstack-scripts/projects/server/booksonic/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  booksonic_stack

exit 0

#
