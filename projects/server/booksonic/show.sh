#!/bin/bash
#
# openstack-scripts/projects/server/booksonic/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  booksonic_stack

exit 0

#
