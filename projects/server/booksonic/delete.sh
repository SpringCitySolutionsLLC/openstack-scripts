#!/bin/bash
#
# openstack-scripts/projects/server/booksonic/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y booksonic_stack

exit 0

#
