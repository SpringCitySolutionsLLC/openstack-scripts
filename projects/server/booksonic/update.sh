#!/bin/bash
#
# openstack-scripts/projects/server/booksonic/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t booksonic.yml \
  booksonic_stack

exit 0

#
