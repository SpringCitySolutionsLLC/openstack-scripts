#!/bin/bash
#
# openstack-scripts/projects/server/booksonic/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t booksonic.yml \
  booksonic_stack

exit 0

#
