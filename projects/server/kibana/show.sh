#!/bin/bash
#
# openstack-scripts/projects/server/kibana/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  kibana_stack

exit 0

#
