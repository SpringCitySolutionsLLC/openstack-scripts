#!/bin/bash
#
# openstack-scripts/projects/server/kibana/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t kibana.yml \
  kibana_stack

exit 0

#
