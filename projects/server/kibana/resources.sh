#!/bin/bash
#
# openstack-scripts/projects/server/kibana/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  kibana_stack

exit 0

#
