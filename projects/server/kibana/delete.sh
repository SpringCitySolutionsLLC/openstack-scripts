#!/bin/bash
#
# openstack-scripts/projects/server/kibana/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y kibana_stack

exit 0

#
