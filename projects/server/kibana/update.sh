#!/bin/bash
#
# openstack-scripts/projects/server/kibana/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t kibana.yml \
  kibana_stack

exit 0

#
