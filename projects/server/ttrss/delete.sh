#!/bin/bash
#
# openstack-scripts/projects/server/ttrss/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y ttrss_stack

exit 0

#
