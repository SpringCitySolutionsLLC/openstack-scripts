#!/bin/bash
#
# openstack-scripts/projects/server/ttrss/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t ttrss.yml \
  ttrss_stack

exit 0

#
