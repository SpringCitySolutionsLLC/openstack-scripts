#!/bin/bash
#
# openstack-scripts/projects/server/ttrss/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t ttrss.yml \
  ttrss_stack

exit 0

#
