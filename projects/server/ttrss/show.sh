#!/bin/bash
#
# openstack-scripts/projects/server/ttrss/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  ttrss_stack

exit 0

#
