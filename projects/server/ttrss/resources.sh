#!/bin/bash
#
# openstack-scripts/projects/server/ttrss/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  ttrss_stack

exit 0

#
