#!/bin/bash
#
# openstack-scripts/projects/server/es22/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y es22_stack

exit 0

#
