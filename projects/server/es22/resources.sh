#!/bin/bash
#
# openstack-scripts/projects/server/es22/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  es22_stack

exit 0

#
