#!/bin/bash
#
# openstack-scripts/projects/server/es22/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t es22.yml \
  es22_stack

exit 0

#
