#!/bin/bash
#
# openstack-scripts/projects/server/es22/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t es22.yml \
  es22_stack

exit 0

#
