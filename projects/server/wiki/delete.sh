#!/bin/bash
#
# openstack-scripts/projects/server/wiki/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y wiki_stack

exit 0

#
