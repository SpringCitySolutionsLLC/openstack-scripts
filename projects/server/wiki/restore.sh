#!/bin/bash
#
# openstack-scripts/projects/server/wiki/restore.sh
#
# See also, backup.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-server-openrc.sh

zun exec wiki /bin/bash -c 'rm -Rf /config/*'

openstack appcontainer cp /root/backup/wiki/config wiki:/

zun exec wiki /bin/bash -c 'chown -R abc:users /config/*'
zun exec wiki /bin/bash -c 'chown root:root /config/custom-cont-init.d'
zun exec wiki /bin/bash -c 'chown root:root /config/custom-services.d'

exit 0

#
