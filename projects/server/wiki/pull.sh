#!/bin/bash
#
# openstack-scripts/projects/server/wiki/pull.sh
#

source /etc/kolla/admin-server-openrc.sh

docker pull linuxserver/dokuwiki

openstack image delete wiki

docker save linuxserver/dokuwiki | \
  openstack image create \
    --container-format docker \
    --disk-format raw \
    --project server \
    wiki

exit 0

#
