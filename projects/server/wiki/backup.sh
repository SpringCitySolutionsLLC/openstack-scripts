#!/bin/bash
#
# openstack-scripts/projects/server/wiki/backup.sh
#
# See also, restore.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-server-openrc.sh

mkdir -p transfer

openstack appcontainer cp wiki:/config transfer

rm -Rf /root/backup/wiki

mkdir -p /root/backup/wiki

mv transfer/config /root/backup/wiki

rm -Rf transfer

exit 0

#
