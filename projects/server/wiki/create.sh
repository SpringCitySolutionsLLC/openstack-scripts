#!/bin/bash
#
# openstack-scripts/projects/server/wiki/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t wiki.yml \
  wiki_stack

exit 0

#
