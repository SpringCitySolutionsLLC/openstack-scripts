#!/bin/bash
#
# openstack-scripts/projects/server/wiki/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  wiki_stack

exit 0

#
