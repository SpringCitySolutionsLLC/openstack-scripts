#!/bin/bash
#
# openstack-scripts/projects/server/wiki/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  wiki_stack

exit 0

#
