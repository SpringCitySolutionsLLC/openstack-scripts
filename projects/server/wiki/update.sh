#!/bin/bash
#
# openstack-scripts/projects/server/wiki/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t wiki.yml \
  wiki_stack

exit 0

#
