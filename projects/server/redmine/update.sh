#!/bin/bash
#
# openstack-scripts/projects/server/redmine/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t redmine.yml \
  redmine_stack

exit 0

#
