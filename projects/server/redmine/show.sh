#!/bin/bash
#
# openstack-scripts/projects/server/redmine/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  redmine_stack

exit 0

#
