#!/bin/bash
#
# openstack-scripts/projects/server/redmine/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t redmine.yml \
  redmine_stack

exit 0

#
