#!/bin/bash
#
# openstack-scripts/projects/server/redmine/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y redmine_stack

exit 0

#
