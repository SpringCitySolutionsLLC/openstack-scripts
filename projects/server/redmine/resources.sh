#!/bin/bash
#
# openstack-scripts/projects/server/redmine/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  redmine_stack

exit 0

#
