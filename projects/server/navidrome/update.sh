#!/bin/bash
#
# openstack-scripts/projects/server/navidrome/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t navidrome.yml \
  navidrome_stack

exit 0

#
