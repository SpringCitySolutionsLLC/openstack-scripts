#!/bin/bash
#
# openstack-scripts/projects/server/navidrome/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  navidrome_stack

exit 0

#
