#!/bin/bash
#
# openstack-scripts/projects/server/navidrome/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  navidrome_stack

exit 0

#
