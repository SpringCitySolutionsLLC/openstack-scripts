#!/bin/bash
#
# openstack-scripts/projects/server/navidrome/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y navidrome_stack

exit 0

#
