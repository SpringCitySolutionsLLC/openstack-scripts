#!/bin/bash
#
# openstack-scripts/projects/server/navidrome/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t navidrome.yml \
  navidrome_stack

exit 0

#
