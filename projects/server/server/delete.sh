#!/bin/bash
#
# openstack-scripts/projects/server/server/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y server_stack

exit 0

#
