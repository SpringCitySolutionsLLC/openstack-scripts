#!/bin/bash
#
# openstack-scripts/projects/server/server/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t server.yml \
  server_stack

exit 0

#
