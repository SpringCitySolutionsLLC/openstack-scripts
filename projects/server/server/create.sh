#!/bin/bash
#
# openstack-scripts/projects/server/server/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t server.yml \
  server_stack

exit 0

#
