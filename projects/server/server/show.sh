#!/bin/bash
#
# openstack-scripts/projects/server/server/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  server_stack

exit 0

#
