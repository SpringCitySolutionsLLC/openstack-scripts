#!/bin/bash
#
# openstack-scripts/projects/server/es12/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t es12.yml \
  es12_stack

exit 0

#
