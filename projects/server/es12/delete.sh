#!/bin/bash
#
# openstack-scripts/projects/server/es12/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y es12_stack

exit 0

#
