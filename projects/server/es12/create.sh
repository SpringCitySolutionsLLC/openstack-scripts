#!/bin/bash
#
# openstack-scripts/projects/server/es12/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t es12.yml \
  es12_stack

exit 0

#
