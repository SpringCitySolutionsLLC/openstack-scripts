#!/bin/bash
#
# openstack-scripts/projects/server/es12/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  es12_stack

exit 0

#
