#!/bin/bash
#
# openstack-scripts/projects/server/es12/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  es12_stack

exit 0

#
