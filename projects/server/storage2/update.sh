#!/bin/bash
#
# openstack-scripts/projects/server/storage2/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t storage2.yml \
  storage2_stack

exit 0

#
