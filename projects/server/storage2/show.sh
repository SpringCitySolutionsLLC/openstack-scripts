#!/bin/bash
#
# openstack-scripts/projects/server/storage2/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  storage2_stack

exit 0

#
