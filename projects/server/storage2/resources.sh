#!/bin/bash
#
# openstack-scripts/projects/server/storage2/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  storage2_stack

exit 0

#
