#!/bin/bash
#
# openstack-scripts/projects/server/storage2/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t storage2.yml \
  storage2_stack

exit 0

#
