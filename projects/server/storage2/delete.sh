#!/bin/bash
#
# openstack-scripts/projects/server/storage2/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y storage2_stack

exit 0

#
