#!/bin/bash
#
# openstack-scripts/projects/server/server2012os2/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t server2012os2.yml \
  server2012os2_stack

exit 0

#
