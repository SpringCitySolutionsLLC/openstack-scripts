#!/bin/bash
#
# openstack-scripts/projects/server/server2012os2/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  server2012os2_stack

exit 0

#
