#!/bin/bash
#
# openstack-scripts/projects/server/server2012os2/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t server2012os2.yml \
  server2012os2_stack

exit 0

#
