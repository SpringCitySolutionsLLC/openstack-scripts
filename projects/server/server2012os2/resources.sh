#!/bin/bash
#
# openstack-scripts/projects/server/server2012os2/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  server2012os2_stack

exit 0

#
