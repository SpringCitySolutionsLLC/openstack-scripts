#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  guacamole_stack

exit 0

#
