#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/check.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack check guacamole_stack

exit 0

#
