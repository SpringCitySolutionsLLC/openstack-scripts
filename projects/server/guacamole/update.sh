#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t guacamole.yml \
  guacamole_stack

exit 0

#
