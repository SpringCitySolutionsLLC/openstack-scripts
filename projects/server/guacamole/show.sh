#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  guacamole_stack

exit 0

#
