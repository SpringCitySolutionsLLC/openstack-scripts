#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t guacamole.yml \
  guacamole_stack

exit 0

#
