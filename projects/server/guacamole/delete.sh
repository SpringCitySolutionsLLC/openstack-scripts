#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y guacamole_stack

exit 0

#
