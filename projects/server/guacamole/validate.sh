#!/bin/bash
#
# openstack-scripts/projects/server/guacamole/validate.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack orchestration template validate -t guacamole.yml 

exit 0

#
