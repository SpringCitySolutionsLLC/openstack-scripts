#!/bin/bash
#
# openstack-scripts/projects/server/backup/validate.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack orchestration template validate -t backup.yml 

exit 0

#
