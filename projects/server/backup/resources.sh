#!/bin/bash
#
# openstack-scripts/projects/server/backup/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  backup_stack

exit 0

#
