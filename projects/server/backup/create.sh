#!/bin/bash
#
# openstack-scripts/projects/server/backup/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t backup.yml \
  backup_stack

exit 0

#
