#!/bin/bash
#
# openstack-scripts/projects/server/backup/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t backup.yml \
  backup_stack

exit 0

#
