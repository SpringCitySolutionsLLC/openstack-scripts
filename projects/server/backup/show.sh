#!/bin/bash
#
# openstack-scripts/projects/server/backup/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  backup_stack

exit 0

#
