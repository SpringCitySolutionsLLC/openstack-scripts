#!/bin/bash
#
# openstack-scripts/projects/server/backup/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y backup_stack

exit 0

#
