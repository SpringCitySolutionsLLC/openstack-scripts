#!/bin/bash
#
# openstack-scripts/projects/server/emby/show.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack show \
  -f yaml \
  emby_stack

exit 0

#
