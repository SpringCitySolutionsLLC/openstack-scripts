#!/bin/bash
#
# openstack-scripts/projects/server/emby/resources.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack resource list \
  -f yaml \
  emby_stack

exit 0

#
