#!/bin/bash
#
# openstack-scripts/projects/server/emby/create.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack create \
  -t emby.yml \
  emby_stack

exit 0

#
