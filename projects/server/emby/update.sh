#!/bin/bash
#
# openstack-scripts/projects/server/emby/update.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack update \
  -t emby.yml \
  emby_stack

exit 0

#
