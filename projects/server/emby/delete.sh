#!/bin/bash
#
# openstack-scripts/projects/server/emby/delete.sh
#

source /etc/kolla/admin-server-openrc.sh

openstack stack delete -y emby_stack

exit 0

#
