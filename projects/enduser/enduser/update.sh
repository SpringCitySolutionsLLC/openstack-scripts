#!/bin/bash
#
# openstack-scripts/projects/enduser/enduser/update.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack update \
  -t enduser.yml \
  enduser_stack

exit 0

#
