#!/bin/bash
#
# openstack-scripts/projects/enduser/enduser/show.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack show \
  -f yaml \
  enduser_stack

exit 0

#
