#!/bin/bash
#
# openstack-scripts/projects/enduser/enduser/delete.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack delete -y enduser_stack

exit 0

#
