#!/bin/bash
#
# openstack-scripts/projects/enduser/enduser/create.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack create \
  -t enduser.yml \
  enduser_stack

exit 0

#
