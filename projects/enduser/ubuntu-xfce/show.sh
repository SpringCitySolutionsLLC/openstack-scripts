#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu-xfce/show.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack show \
  -f yaml \
  ubuntu-xfce_stack

exit 0

#
