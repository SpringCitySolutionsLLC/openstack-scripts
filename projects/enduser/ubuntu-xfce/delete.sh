#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu-xfce/delete.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack delete -y ubuntu-xfce_stack

exit 0

#
