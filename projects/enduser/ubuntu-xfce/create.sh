#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu-xfce/create.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack create \
  -t ubuntu-xfce.yml \
  ubuntu-xfce_stack

exit 0

#
