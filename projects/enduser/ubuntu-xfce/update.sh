#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu-xfce/update.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack update \
  -t ubuntu-xfce.yml \
  ubuntu-xfce_stack

exit 0

#
