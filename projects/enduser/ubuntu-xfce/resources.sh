#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu-xfce/resources.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack resource list \
  -f yaml \
  ubuntu-xfce_stack

exit 0

#
