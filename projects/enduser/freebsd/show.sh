#!/bin/bash
#
# openstack-scripts/projects/enduser/freebsd/show.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack show \
  -f yaml \
  freebsd_stack

exit 0

#
