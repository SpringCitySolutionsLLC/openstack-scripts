#!/bin/bash
#
# openstack-scripts/projects/enduser/freebsd/delete.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack delete -y freebsd_stack

exit 0

#
