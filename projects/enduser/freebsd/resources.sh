#!/bin/bash
#
# openstack-scripts/projects/enduser/freebsd/resources.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack resource list \
  -f yaml \
  freebsd_stack

exit 0

#
