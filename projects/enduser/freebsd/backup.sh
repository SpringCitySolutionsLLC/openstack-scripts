#!/bin/bash
#
# openstack-scripts/projects/enduser/freebsd/backup.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack container create \
  backup-freebsd

openstack server stop \
  freebsd

sleep 120

openstack volume backup create \
  --container backup-freebsd \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  freebsd

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume freebsd | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-freebsd METADATA

rm METADATA

openstack server start \
  freebsd

# This is where you rclone out the contents of container "backup-freebsd" into a NFS directory for backup

exit 0

#
