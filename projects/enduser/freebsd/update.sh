#!/bin/bash
#
# openstack-scripts/projects/enduser/freebsd/update.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack update \
  -t freebsd.yml \
  freebsd_stack

exit 0

#
