#!/bin/bash
#
# openstack-scripts/projects/enduser/freebsd/create.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack create \
  -t freebsd.yml \
  freebsd_stack

exit 0

#
