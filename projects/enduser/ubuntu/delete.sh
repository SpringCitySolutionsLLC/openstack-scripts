#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu/delete.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack delete -y ubuntu_stack

exit 0

#
