#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu/show.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack show \
  -f yaml \
  ubuntu_stack

exit 0

#
