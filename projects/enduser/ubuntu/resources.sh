#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu/resources.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack resource list \
  -f yaml \
  ubuntu_stack

exit 0

#
