#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu/create.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack create \
  -t ubuntu.yml \
  ubuntu_stack

exit 0

#
