#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu/update.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack stack update \
  -t ubuntu.yml \
  ubuntu_stack

exit 0

#
