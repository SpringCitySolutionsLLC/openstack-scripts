#!/bin/bash
#
# openstack-scripts/projects/enduser/ubuntu/backup.sh
#

source /etc/kolla/admin-enduser-openrc.sh

openstack container create \
  backup-ubuntu

openstack server stop \
  ubuntu

sleep 120

openstack volume backup create \
  --container backup-ubuntu \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  ubuntu

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume ubuntu | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-ubuntu METADATA

rm METADATA

openstack server start \
  ubuntu

# This is where you rclone out the contents of container "backup-ubuntu" into a NFS directory for backup

exit 0

#
