#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc11/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dc11_stack

exit 0

#
