#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc11/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dc11.yml \
  dc11_stack

exit 0

#
