#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc11/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dc11_stack

exit 0

#
