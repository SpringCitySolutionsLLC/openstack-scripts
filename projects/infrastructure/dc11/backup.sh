#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc11/backup.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack container create \
  backup-dc11

openstack server stop \
  dc11

sleep 120

openstack volume backup create \
  --container backup-dc11 \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  dc11

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume dc11 | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-dc11 METADATA

rm METADATA

openstack server start \
  dc11

# This is where you rclone out the contents of container "backup-dc11" into a NFS directory for backup

exit 0

#
