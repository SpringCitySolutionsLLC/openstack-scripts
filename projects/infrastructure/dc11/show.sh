#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc11/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dc11_stack

exit 0

#
