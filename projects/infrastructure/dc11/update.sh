#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc11/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dc11.yml \
  dc11_stack

exit 0

#
