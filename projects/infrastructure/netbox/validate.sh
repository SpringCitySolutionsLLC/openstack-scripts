#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbox/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t netbox.yml 

exit 0

#
