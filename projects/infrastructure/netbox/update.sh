#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbox/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t netbox.yml \
  netbox_stack

exit 0

#
