#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbox/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  netbox_stack

exit 0

#
