#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbox/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t netbox.yml \
  netbox_stack

exit 0

#
