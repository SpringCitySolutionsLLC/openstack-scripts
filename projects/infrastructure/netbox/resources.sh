#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbox/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  netbox_stack

exit 0

#
