#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbox/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y netbox_stack

exit 0

#
