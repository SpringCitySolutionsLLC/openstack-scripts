#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/check.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack check portainer_stack

exit 0

#
