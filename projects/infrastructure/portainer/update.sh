#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t portainer.yml \
  portainer_stack

exit 0

#
