#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y portainer_stack

exit 0

#
