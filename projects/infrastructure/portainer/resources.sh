#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  portainer_stack

exit 0

#
