#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  portainer_stack

exit 0

#
