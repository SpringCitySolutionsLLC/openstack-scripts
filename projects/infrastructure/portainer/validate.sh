#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t portainer.yml 

exit 0

#
