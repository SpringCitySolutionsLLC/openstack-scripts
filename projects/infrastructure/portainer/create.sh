#!/bin/bash
#
# openstack-scripts/projects/infrastructure/portainer/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t portainer.yml \
  portainer_stack

exit 0

#
