#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixmysql/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t zabbixmysql.yml \
  zabbixmysql_stack

exit 0

#
