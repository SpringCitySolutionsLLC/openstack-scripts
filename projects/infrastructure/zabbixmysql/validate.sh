#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixmysql/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t zabbixmysql.yml 

exit 0

#
