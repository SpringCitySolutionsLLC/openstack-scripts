#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixmysql/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  zabbixmysql_stack

exit 0

#
