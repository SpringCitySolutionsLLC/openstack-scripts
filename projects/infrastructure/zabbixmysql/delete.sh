#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixmysql/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y zabbixmysql_stack

exit 0

#
