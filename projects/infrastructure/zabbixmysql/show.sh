#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixmysql/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  zabbixmysql_stack

exit 0

#
