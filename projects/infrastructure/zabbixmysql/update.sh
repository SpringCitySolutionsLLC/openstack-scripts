#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixmysql/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t zabbixmysql.yml \
  zabbixmysql_stack

exit 0

#
