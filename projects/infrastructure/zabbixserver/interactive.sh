#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/interactive.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun exec --interactive zabbixserver /bin/bash

exit 0

#
