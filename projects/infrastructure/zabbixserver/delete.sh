#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y zabbixserver_stack

exit 0

#
