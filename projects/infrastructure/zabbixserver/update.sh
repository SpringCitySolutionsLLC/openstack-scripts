#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t zabbixserver.yml \
  zabbixserver_stack

exit 0

#
