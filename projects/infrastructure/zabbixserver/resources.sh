#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  zabbixserver_stack

exit 0

#
