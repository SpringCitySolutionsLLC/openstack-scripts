#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t zabbixserver.yml 

exit 0

#
