#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t zabbixserver.yml \
  zabbixserver_stack

exit 0

#
