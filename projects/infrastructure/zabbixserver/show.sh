#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  zabbixserver_stack

exit 0

#
