#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixserver/restart.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun restart zabbixserver

exit 0

#
