#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/check.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack check zerotier_stack

exit 0

#
