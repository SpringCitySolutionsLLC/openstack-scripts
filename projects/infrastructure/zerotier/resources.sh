#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  zerotier_stack

exit 0

#
