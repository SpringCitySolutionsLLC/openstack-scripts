#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t zerotier.yml \
  zerotier_stack

exit 0

#
