#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t zerotier.yml 

exit 0

#
