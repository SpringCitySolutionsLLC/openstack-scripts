#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  zerotier_stack

exit 0

#
