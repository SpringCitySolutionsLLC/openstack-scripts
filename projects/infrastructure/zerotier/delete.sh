#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y zerotier_stack

exit 0

#
