#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zerotier/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t zerotier.yml \
  dns11_stack

exit 0

#
