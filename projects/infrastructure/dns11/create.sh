#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns11/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dns11.yml \
  dns11_stack

exit 0

#
