#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns11/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t dns11.yml 

exit 0

#
