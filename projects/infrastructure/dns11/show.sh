#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns11/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dns11_stack

exit 0

#
