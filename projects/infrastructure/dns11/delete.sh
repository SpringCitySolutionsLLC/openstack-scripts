#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns11/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dns11_stack

exit 0

#
