#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns11/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dns11.yml \
  dns11_stack

exit 0

#
