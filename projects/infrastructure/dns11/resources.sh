#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns11/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dns11_stack

exit 0

#
