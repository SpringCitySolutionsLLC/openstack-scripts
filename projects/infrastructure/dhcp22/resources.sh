#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp22/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dhcp22_stack

exit 0

#
