#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp22/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dhcp22.yml \
  dhcp22_stack

exit 0

#
