#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp22/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dhcp22.yml \
  dhcp22_stack

exit 0

#
