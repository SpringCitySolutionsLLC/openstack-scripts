#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp22/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dhcp22_stack

exit 0

#
