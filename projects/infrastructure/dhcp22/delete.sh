#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp22/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dhcp22_stack

exit 0

#
