#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  zabbixproxy_stack

exit 0

#
