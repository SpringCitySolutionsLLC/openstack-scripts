#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t zabbixproxy.yml \
  zabbixproxy_stack

exit 0

#
