#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/interactive.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun exec --interactive zabbixproxy /bin/bash

exit 0

#
