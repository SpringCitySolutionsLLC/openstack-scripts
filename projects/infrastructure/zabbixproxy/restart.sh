#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/restart.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun restart zabbixproxy

exit 0

#
