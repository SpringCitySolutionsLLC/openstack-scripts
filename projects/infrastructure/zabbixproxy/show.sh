#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  zabbixproxy_stack

exit 0

#
