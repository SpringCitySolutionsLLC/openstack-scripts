#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y zabbixproxy_stack

exit 0

#
