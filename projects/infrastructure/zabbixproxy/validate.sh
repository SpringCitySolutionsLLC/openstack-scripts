#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t zabbixproxy.yml 

exit 0

#
