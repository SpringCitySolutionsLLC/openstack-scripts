#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixproxy/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t zabbixproxy.yml \
  zabbixproxy_stack

exit 0

#
