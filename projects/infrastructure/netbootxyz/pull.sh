#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/pull.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

docker pull ghcr.io/netbootxyz/netbootxyz

openstack image delete netbootxyz

docker save ghcr.io/netbootxyz/netbootxyz | \
  openstack image create \
    --container-format docker \
    --disk-format raw \
    --project infrastructure \
    netbootxyz

exit 0

#
