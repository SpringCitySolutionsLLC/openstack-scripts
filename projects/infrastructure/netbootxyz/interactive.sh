#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/interactive.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun exec --interactive netbootxyz /bin/bash

exit 0

#
