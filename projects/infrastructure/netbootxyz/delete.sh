#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y netbootxyz_stack

exit 0

#
