#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  netbootxyz_stack

exit 0

#
