#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  netbootxyz_stack

exit 0

#
