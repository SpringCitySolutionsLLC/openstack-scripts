#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t netbootxyz.yml \
  netbootxyz_stack

exit 0

#
