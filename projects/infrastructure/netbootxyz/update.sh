#!/bin/bash
#
# openstack-scripts/projects/infrastructure/netbootxyz/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t netbootxyz.yml \
  netbootxyz_stack

exit 0

#
