#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t dns21.yml 

exit 0

#
