#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dns21_stack

exit 0

#
