#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dns21.yml \
  dns21_stack

exit 0

#
