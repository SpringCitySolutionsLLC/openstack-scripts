#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/check.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack check dns21_stack

exit 0

#
