#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dns21.yml \
  dns21_stack

exit 0

#
