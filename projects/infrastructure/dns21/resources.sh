#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dns21_stack

exit 0

#
