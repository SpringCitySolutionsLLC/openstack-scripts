#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dns21_stack

exit 0

#
