#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns21/backup.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack container create \
  backup-dns21

openstack server stop \
  dns21

sleep 120

openstack volume backup create \
  --container backup-dns21 \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  dns21

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume dns21 | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-dns21 METADATA

rm METADATA

openstack server start \
  dns21

# This is where you rclone out the contents of container "backup-dns21" into a NFS directory for backup

exit 0

#
