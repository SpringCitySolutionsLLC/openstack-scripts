#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y observium_stack

exit 0

#
