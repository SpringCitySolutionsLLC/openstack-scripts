#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/backup.sh
#
# See also, restore.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-infrastructure-openrc.sh

mkdir -p transfer

openstack appcontainer cp observium:/config/config.php transfer

rm -Rf /root/backup/observium

mkdir -p /root/backup/observium

mv transfer/* /root/backup/observium

rm -Rf transfer

exit 0

#
