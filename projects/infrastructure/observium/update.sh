#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t observium.yml \
  observium_stack

exit 0

#
