#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t observium.yml 

exit 0

#
