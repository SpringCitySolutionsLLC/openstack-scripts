#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  observium_stack

exit 0

#
