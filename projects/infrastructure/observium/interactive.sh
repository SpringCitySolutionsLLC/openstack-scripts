#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/interactive.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun exec --interactive observium /bin/bash

exit 0

#
