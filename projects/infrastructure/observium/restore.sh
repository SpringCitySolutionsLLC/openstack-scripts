#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/restore.sh
#
# See also, backup.sh
#
# Files stored in ~/backup are magically backed up and restored at the NAS level

source /etc/kolla/admin-infrastructure-openrc.sh

openstack appcontainer cp /root/backup/observium/config.php observium:/config

zun exec observium /bin/bash -c 'chown -R nobody:users /config/*'

exit 0

#
