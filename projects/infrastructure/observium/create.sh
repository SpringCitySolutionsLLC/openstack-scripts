#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t observium.yml \
  observium_stack

exit 0

#
