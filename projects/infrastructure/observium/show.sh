#!/bin/bash
#
# openstack-scripts/projects/infrastructure/observium/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  observium_stack

exit 0

#
