#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t dc12.yml 

exit 0

#
