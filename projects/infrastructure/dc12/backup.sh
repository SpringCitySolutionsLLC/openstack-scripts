#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/backup.sh
#

source /etc/kolla/admin-infrasrructure-openrc.sh

openstack container create \
  backup-dc12

openstack server stop \
  dc12

sleep 120

openstack volume backup create \
  --container backup-dc12 \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  dc12

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume dc12 | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-dc12 METADATA

rm METADATA

openstack server start \
  dc12

# This is where you rclone out the contents of container "backup-dc12" into a NFS directory for backup

exit 0

#
