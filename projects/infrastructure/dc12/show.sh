#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dc12_stack

exit 0

#
