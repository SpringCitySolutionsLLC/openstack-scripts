#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dc12_stack

exit 0

#
