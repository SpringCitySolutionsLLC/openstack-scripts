#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dc12.yml \
  dc12_stack

exit 0

#
