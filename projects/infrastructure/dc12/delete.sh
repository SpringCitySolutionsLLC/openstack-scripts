#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dc12_stack

exit 0

#
