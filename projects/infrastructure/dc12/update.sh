#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc12/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dc12.yml \
  dc12_stack

exit 0

#
