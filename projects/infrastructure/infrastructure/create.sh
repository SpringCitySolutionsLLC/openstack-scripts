#!/bin/bash
#
# openstack-scripts/projects/infrastructure/infrastructure/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t infrastructure.yml \
  infrastructure_stack

exit 0

#
