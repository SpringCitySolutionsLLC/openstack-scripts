#!/bin/bash
#
# openstack-scripts/projects/infrastructure/infrastructure/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t infrastructure.yml \
  infrastructure_stack

exit 0

#
