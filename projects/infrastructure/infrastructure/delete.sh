#!/bin/bash
#
# openstack-scripts/projects/infrastructure/infrastructure/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y infrastructure_stack

exit 0

#
