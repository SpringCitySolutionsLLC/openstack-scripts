#!/bin/bash
#
# openstack-scripts/projects/infrastructure/infrastructure/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  infrastructure_stack

exit 0

#
