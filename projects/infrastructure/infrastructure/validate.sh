#!/bin/bash
#
# openstack-scripts/projects/infrastructure/infrastructure/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t infrastructure.yml 

exit 0

#
