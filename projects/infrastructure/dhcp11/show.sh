#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp11/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dhcp11_stack

exit 0

#
