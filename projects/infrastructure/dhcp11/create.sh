#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp11/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dhcp11.yml \
  dhcp11_stack

exit 0

#
