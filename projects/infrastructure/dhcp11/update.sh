#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp11/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dhcp11.yml \
  dhcp11_stack

exit 0

#
