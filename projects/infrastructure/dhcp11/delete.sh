#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp11/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dhcp11_stack

exit 0

#
