#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp11/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dhcp11_stack

exit 0

#
