#!/bin/bash
#
# openstack-scripts/projects/infrastructure/unifi/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  unifi_stack

exit 0

#
