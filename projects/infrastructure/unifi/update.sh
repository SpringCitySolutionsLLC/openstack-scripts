#!/bin/bash
#
# openstack-scripts/projects/infrastructure/unifi/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t unifi.yml \
  unifi_stack

exit 0

#
