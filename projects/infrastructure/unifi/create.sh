#!/bin/bash
#
# openstack-scripts/projects/infrastructure/unifi/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t unifi.yml \
  unifi_stack

exit 0

#
