#!/bin/bash
#
# openstack-scripts/projects/infrastructure/unifi/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y unifi_stack

exit 0

#
