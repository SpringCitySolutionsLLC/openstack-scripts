#!/bin/bash
#
# openstack-scripts/projects/infrastructure/unifi/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  unifi_stack

exit 0

#
