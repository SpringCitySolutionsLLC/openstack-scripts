#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t zabbixweb.yml \
  zabbixweb_stack

exit 0

#
