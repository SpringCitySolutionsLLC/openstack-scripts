#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/interactive.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun exec --interactive zabbixweb /bin/bash

exit 0

#
