#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y zabbixweb_stack

exit 0

#
