#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  zabbixweb_stack

exit 0

#
