#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t zabbixweb.yml 

exit 0

#
