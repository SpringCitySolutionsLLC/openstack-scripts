#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t zabbixweb.yml \
  zabbixweb_stack

exit 0

#
