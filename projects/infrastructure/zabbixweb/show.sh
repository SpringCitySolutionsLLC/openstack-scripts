#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixweb/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  zabbixweb_stack

exit 0

#
