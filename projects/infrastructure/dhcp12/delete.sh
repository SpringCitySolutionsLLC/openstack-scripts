#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp12/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dhcp12_stack

exit 0

#
