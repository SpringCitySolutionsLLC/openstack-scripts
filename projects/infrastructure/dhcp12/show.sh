#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp12/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dhcp12_stack

exit 0

#
