#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp12/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dhcp12.yml \
  dhcp12_stack

exit 0

#
