#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp12/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dhcp12.yml \
  dhcp12_stack

exit 0

#
