#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp12/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dhcp12_stack

exit 0

#
