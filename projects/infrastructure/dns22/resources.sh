#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dns22_stack

exit 0

#
