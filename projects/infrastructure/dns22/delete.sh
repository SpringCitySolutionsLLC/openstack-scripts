#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dns22_stack

exit 0

#
