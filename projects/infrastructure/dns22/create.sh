#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dns22.yml \
  dns22_stack

exit 0

#
