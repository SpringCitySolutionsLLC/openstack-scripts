#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dns22_stack

exit 0

#
