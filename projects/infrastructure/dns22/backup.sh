#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/backup.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack container create \
  backup-dns22

openstack server stop \
  dns22

sleep 120

openstack volume backup create \
  --container backup-dns22 \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  dns22

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume dns22 | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-dns22 METADATA

rm METADATA

openstack server start \
  dns22

# This is where you rclone out the contents of container "backup-dns22" into a NFS directory for backup

exit 0

#
