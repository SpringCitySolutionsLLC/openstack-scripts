#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t dns22.yml 

exit 0

#
