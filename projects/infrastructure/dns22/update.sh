#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns22/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dns22.yml \
  dns22_stack

exit 0

#
