#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t zabbixadminer.yml \
  zabbixadminer_stack

exit 0

#
