#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/check.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack check zabbixadminer_stack

exit 0

#
