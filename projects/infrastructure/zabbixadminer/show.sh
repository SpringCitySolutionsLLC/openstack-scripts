#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  zabbixadminer_stack

exit 0

#
