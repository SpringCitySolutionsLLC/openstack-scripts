#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/restart.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun restart zabbixadminer

exit 0

#
