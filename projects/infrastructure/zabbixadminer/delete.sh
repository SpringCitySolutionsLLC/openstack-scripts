#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y zabbixadminer_stack

exit 0

#
