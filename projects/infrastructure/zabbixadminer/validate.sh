#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t zabbixadminer.yml 

exit 0

#
