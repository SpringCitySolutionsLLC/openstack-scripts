#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/interactive.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

zun exec --interactive zabbixadminer /bin/sh

exit 0

#
