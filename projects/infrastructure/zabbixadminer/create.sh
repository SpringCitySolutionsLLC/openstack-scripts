#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t zabbixadminer.yml \
  zabbixadminer_stack

exit 0

#
