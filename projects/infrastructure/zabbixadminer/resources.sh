#!/bin/bash
#
# openstack-scripts/projects/infrastructure/zabbixadminer/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  zabbixadminer_stack

exit 0

#
