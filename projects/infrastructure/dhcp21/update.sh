#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp21/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dhcp21.yml \
  dhcp21_stack

exit 0

#
