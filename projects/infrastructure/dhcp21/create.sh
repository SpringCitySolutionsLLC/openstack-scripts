#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp21/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dhcp21.yml \
  dhcp21_stack

exit 0

#
