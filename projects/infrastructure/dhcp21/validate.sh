#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp21/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t dhcp21.yml 

exit 0

#
