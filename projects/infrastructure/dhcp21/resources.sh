#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp21/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dhcp21_stack

exit 0

#
