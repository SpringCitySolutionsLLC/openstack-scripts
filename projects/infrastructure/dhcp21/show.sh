#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp21/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dhcp21_stack

exit 0

#
