#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dhcp21/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dhcp21_stack

exit 0

#
