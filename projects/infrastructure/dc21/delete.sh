#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc21/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dc21_stack

exit 0

#
