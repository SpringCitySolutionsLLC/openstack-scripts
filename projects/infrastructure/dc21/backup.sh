#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc21/backup.sh
#

source /etc/kolla/admin-infrasrructure-openrc.sh

openstack container create \
  backup-dc21

openstack server stop \
  dc21

sleep 120

openstack volume backup create \
  --container backup-dc21 \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  dc21

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume dc21 | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-dc21 METADATA

rm METADATA

openstack server start \
  dc21

# This is where you rclone out the contents of container "backup-dc21" into a NFS directory for backup

exit 0

#
