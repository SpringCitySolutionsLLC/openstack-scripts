#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc21/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dc21.yml \
  dc21_stack

exit 0

#
