#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc21/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dc21_stack

exit 0

#
