#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc21/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dc21.yml \
  dc21_stack

exit 0

#
