#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc21/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dc21_stack

exit 0

#
