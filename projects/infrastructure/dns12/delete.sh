#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns12/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dns12_stack

exit 0

#
