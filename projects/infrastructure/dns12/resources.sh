#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns12/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dns12_stack

exit 0

#
