#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns12/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dns12.yml \
  dns12_stack

exit 0

#
