#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns12/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dns12_stack

exit 0

#
