#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns12/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t dns12.yml 

exit 0

#
