#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dns12/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dns12.yml \
  dns12_stack

exit 0

#
