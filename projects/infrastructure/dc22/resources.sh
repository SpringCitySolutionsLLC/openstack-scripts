#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc22/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  dc22_stack

exit 0

#
