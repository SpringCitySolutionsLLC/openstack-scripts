#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc22/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y dc22_stack

exit 0

#
