#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc22/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  dc22_stack

exit 0

#
