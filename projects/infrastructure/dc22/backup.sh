#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc22/backup.sh
#

source /etc/kolla/admin-infrasrructure-openrc.sh

openstack container create \
  backup-dc22

openstack server stop \
  dc22

sleep 120

openstack volume backup create \
  --container backup-dc22 \
  --no-incremental \
  --description `date --iso-8601` \
  --force \
  dc22

sleep 1200

export BACKUP_ID=$(openstack volume backup list --volume dc22 | grep available | awk '{ print $2 }')

echo BACKUP_ID is $BACKUP_ID

cinder backup-export $BACKUP_ID > METADATA

openstack object create backup-dc22 METADATA

rm METADATA

openstack server start \
  dc22

# This is where you rclone out the contents of container "backup-dc22" into a NFS directory for backup

exit 0

#
