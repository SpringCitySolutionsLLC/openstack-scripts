#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc22/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t dc22.yml \
  dc22_stack

exit 0

#
