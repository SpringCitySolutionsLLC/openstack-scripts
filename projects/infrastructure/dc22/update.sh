#!/bin/bash
#
# openstack-scripts/projects/infrastructure/dc22/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t dc22.yml \
  dc22_stack

exit 0

#
