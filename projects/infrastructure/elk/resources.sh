#!/bin/bash
#
# openstack-scripts/projects/infrastructure/elk/resources.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack resource list \
  -f yaml \
  elk_stack

exit 0

#
