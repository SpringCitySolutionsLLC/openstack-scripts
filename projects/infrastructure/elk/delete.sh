#!/bin/bash
#
# openstack-scripts/projects/infrastructure/elk/delete.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack delete -y elk_stack

exit 0

#
