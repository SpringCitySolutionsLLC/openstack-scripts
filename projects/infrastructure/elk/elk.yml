heat_template_version: 2015-10-15
description: elk.cedar.mulhollon.com

resources:

  my_volume:
    type: OS::Cinder::Volume
    properties:
      size: 16
      name: elk
      image: ubuntu-20.04

  my_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: "prod-net"
      floating_ip_address: "10.10.7.23"
      port_id: { get_resource: my_port }

  my_forward_dns_os1:
    type: OS::Designate::RecordSet
    properties:
      type: "A"
      zone: "infrastructure.os1.cedar.mulhollon.net."
      name: "elk"
      records:
        - get_attr: [ my_floating_ip, floating_ip_address ]

  my_forward_dns_os2:
    type: OS::Designate::RecordSet
    properties:
      type: "A"
      zone: "infrastructure.os2.cedar.mulhollon.net."
      name: "elk"
      records:
        - get_attr: [ my_floating_ip, floating_ip_address ]

  my_port:
    type: OS::Neutron::Port
    properties:
      network: infrastructure-net
      name: elk
      fixed_ips:
        - subnet_id: "infrastructure-v4"
      security_groups:
        - { get_resource: my_elasticsearch_json_sg }
        - { get_resource: my_elasticsearch_monitor_sg }
        - { get_resource: my_elasticsearch_transport_sg }
        - { get_resource: my_filebeat_sg }
        - { get_resource: my_kibana_sg }
        - ssh-infrastructure
        - { get_resource: my_syslog_rfc3164_udp_sg }
        - { get_resource: my_syslog_rfc3164_tcp_sg }
        - { get_resource: my_syslog_rfc5424_udp_sg }
        - { get_resource: my_syslog_rfc5424_tcp_sg }
        - ping-infrastructure
        - portainer-infrastructure
        - zabbix-infrastructure

  my_elasticsearch_json_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: elasticsearch-json-elk-infrastructure
      description: "Elasticsearch JSON"

  my_elasticsearch_json_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_elasticsearch_json_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 9200
      port_range_max: 9200
      remote_ip_prefix: "0.0.0.0/0"

  my_elasticsearch_monitor_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: elasticsearch-monitor-elk-infrastructure
      description: "Elasticsearch Monitor API"

  my_elasticsearch_monitor_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_elasticsearch_monitor_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 9600
      port_range_max: 9600
      remote_ip_prefix: "0.0.0.0/0"

  my_elasticsearch_transport_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: elasticsearch-transport-elk-infrastructure
      description: "Elasticsearch Transport"

  my_elasticsearch_transport_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_elasticsearch_transport_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 9300
      port_range_max: 9300
      remote_ip_prefix: "0.0.0.0/0"

  my_filebeat_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: filebeat-elk-infrastructure
      description: "Filebeat"

  my_filebeat_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_filebeat_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 5044
      port_range_max: 5044
      remote_ip_prefix: "0.0.0.0/0"

  my_kibana_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: kibana-elk-infrastructure
      description: "Kibana Web UI"

  my_kibana_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_kibana_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 5601
      port_range_max: 5601
      remote_ip_prefix: "0.0.0.0/0"

  my_syslog_rfc3164_udp_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: syslog-rfc3164-udp-elk-infrastructure
      description: "Syslog RFC3164 UDP"

  my_syslog_rfc3164_udp_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_syslog_rfc3164_udp_sg }
      direction: ingress
      protocol: udp
      port_range_min: 23164
      port_range_max: 23164
      remote_ip_prefix: "0.0.0.0/0"

  my_syslog_rfc3164_tcp_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: syslog-rfc3164-tcp-elk-infrastructure
      description: "Syslog RFC3164 TCP"

  my_syslog_rfc3164_tcp_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_syslog_rfc3164_tcp_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 23164
      port_range_max: 23164
      remote_ip_prefix: "0.0.0.0/0"

  my_syslog_rfc5424_udp_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: syslog-rfc5424-udp-elk-infrastructure
      description: "Syslog RFC5424 UDP"

  my_syslog_rfc5424_udp_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_syslog_rfc5424_udp_sg }
      direction: ingress
      protocol: udp
      port_range_min: 25424
      port_range_max: 25424
      remote_ip_prefix: "0.0.0.0/0"

  my_syslog_rfc5424_tcp_sg:
    type: OS::Neutron::SecurityGroup
    properties:
      name: syslog-rfc5424-tcp-elk-infrastructure
      description: "Syslog RFC5424 TCP"

  my_syslog_rfc5424_tcp_sgr_1:
    type: OS::Neutron::SecurityGroupRule
    properties:
      security_group:  { get_resource: my_syslog_rfc5424_tcp_sg }
      direction: ingress
      protocol: tcp
      port_range_min: 25424
      port_range_max: 25424
      remote_ip_prefix: "0.0.0.0/0"

  my_instance:
    type: OS::Nova::Server
    properties:
      flavor: 1-cpu-08G-ram-32G-disk
      key_name: ansible
      name: elk
      networks:
        - port: { get_resource: my_port }
      block_device_mapping:
        - device_name: vda
          volume_id: { get_resource: my_volume }
          delete_on_termination: false

outputs:
  floating_ip:
    description: IP address
    value: { get_attr: [ my_floating_ip, floating_ip_address ] }
