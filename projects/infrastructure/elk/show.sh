#!/bin/bash
#
# openstack-scripts/projects/infrastructure/elk/show.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack show \
  -f yaml \
  elk_stack

exit 0

#
