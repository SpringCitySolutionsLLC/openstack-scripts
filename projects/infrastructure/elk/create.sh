#!/bin/bash
#
# openstack-scripts/projects/infrastructure/elk/create.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack create \
  -t elk.yml \
  elk_stack

exit 0

#
