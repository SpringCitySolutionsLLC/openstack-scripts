#!/bin/bash
#
# openstack-scripts/projects/infrastructure/elk/update.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack stack update \
  -t elk.yml \
  elk_stack

exit 0

#
