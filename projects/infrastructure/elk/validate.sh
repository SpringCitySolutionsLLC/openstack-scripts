#!/bin/bash
#
# openstack-scripts/projects/infrastructure/elk/validate.sh
#

source /etc/kolla/admin-infrastructure-openrc.sh

openstack orchestration template validate -t elk.yml 

exit 0

#
