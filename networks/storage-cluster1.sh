#!/bin/bash
#
# openstack-scripts/networks/storage-cluster1.sh
#

source /etc/kolla/admin-openrc.sh

openstack network create \
  --share \
  --external \
  --provider-network-type vlan \
  --provider-physical-network physnet1 \
  --provider-segment 30 \
  --disable-port-security \
  --mtu 8000 \
  storage-net

# prod-cluster1.sh and prod-cluster2.sh have different allocation pools on same LAN as seen in netbox

openstack subnet create \
  --no-dhcp \
  --ip-version 4 \
  --subnet-range 10.30.0.0/16 \
  --allocation-pool start=10.30.248.2,end=10.30.251.253 \
  --gateway 10.30.1.1 \
  --dns-nameserver 10.10.7.3 \
  --dns-nameserver 10.10.7.4 \
  --network storage-net \
  storage-net-v4

# TODO: prod-net-v6 definition belongs here

exit 0
