#!/bin/bash
#
# openstack-scripts/networks/prod-cluster2.sh
#

source /etc/kolla/admin-openrc.sh

# Note that only prod-net should be set default, all others skip that line

openstack network create \
  --share \
  --external \
  --provider-network-type vlan \
  --provider-physical-network physnet1 \
  --provider-segment 10 \
  --disable-port-security \
  --mtu 8000 \
  --default \
  prod-net

# prod-cluster1.sh and prod-cluster2.sh have different allocation pools on same LAN as seen in netbox

openstack subnet create \
  --no-dhcp \
  --ip-version 4 \
  --subnet-range 10.10.0.0/16 \
  --allocation-pool start=10.10.244.2,end=10.10.247.253 \
  --gateway 10.10.1.1 \
  --dns-nameserver 10.10.250.168 \
  --dns-nameserver 10.10.249.196 \
  --network prod-net \
  prod-net-v4

# TODO: prod-net-v6 definition belongs here

exit 0
