#!/bin/bash
#
# openstack-scripts/networks/overlay-cluster2.sh
#

source /etc/kolla/admin-openrc.sh

openstack network create \
  --share \
  --external \
  --provider-network-type vlan \
  --provider-physical-network physnet1 \
  --provider-segment 60 \
  --disable-port-security \
  --mtu 8000 \
  overlay-net

# prod-cluster1.sh and prod-cluster2.sh have different allocation pools on same LAN as seen in netbox

openstack subnet create \
  --no-dhcp \
  --ip-version 4 \
  --subnet-range 10.60.0.0/16 \
  --allocation-pool start=10.60.244.2,end=10.60.247.253 \
  --gateway 10.60.1.1 \
  --dns-nameserver 10.10.250.168 \
  --dns-nameserver 10.10.249.196 \
  --network overlay-net \
  overlay-net-v4

# TODO: prod-net-v6 definition belongs here

exit 0
