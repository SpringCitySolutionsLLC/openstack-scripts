#!/bin/bash
#
# openstack-scripts/networks/isp-cluster2.sh
#

source /etc/kolla/admin-openrc.sh

openstack network create \
  --share \
  --external \
  --provider-network-type vlan \
  --provider-physical-network physnet1 \
  --provider-segment 50 \
  --disable-port-security \
  --mtu 8000 \
  isp-net

exit 0
