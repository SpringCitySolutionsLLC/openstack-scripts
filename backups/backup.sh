#!/bin/bash
#
# openstack-scripts/backups/backup.sh
# 

mkdir -p /root/openstack-scripts/backups/$HOSTNAME

cp /root/ansible.cfg /root/openstack-scripts/backups/$HOSTNAME/ansible.cfg

cp /root/multinode /root/openstack-scripts/backups/$HOSTNAME/multinode

mkdir -p /root/openstack-scripts/backups/$HOSTNAME/globals.d
cp /etc/kolla/globals.d/* /root/openstack-scripts/backups/$HOSTNAME/globals.d

mkdir -p /root/openstack-scripts/backups/$HOSTNAME/passwords
cp /etc/kolla/passwords.yml /root/openstack-scripts/backups/$HOSTNAME/passwords
echo "passwords.yml" > /root/openstack-scripts/backups/$HOSTNAME/passwords/.gitignore

mkdir -p /root/openstack-scripts/backups/$HOSTNAME/configs
cp -R /etc/kolla/config/* /root/openstack-scripts/backups/$HOSTNAME/configs

mkdir -p /root/openstack-scripts/backups/$HOSTNAME/netplan
cp -R /etc/netplan/netplan.yaml /root/openstack-scripts/backups/$HOSTNAME/netplan

#
