#!/bin/bash
#
# openstack-scripts/installcli/installcli.sh
#

# Test for presence of venv, activate if not present
if [[ -z $VIRTUAL_ENV ]]; then
  # This is how I activate my kolla-ansible venv.
  echo Activating Kolla-Ansible venv
  source /root/kolla-ansible/bin/activate
else
  echo Kolla-Ansible venv already activated, very cool.
fi

pip install python-barbicanclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-cinderclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-designateclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-glanceclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-heatclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-mistralclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-neutronclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-novaclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-swiftclient -c https://releases.openstack.org/constraints/upper/yoga

pip install python-zunclient -c https://releases.openstack.org/constraints/upper/yoga

exit 0

#
