#!/bin/bash
#
# openstack-scripts/demos/mistral/hello/execution-list.sh
#

source /etc/kolla/admin-openrc.sh

openstack workflow execution list \
  -f yaml

exit 0

#
