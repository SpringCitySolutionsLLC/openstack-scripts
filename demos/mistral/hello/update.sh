#!/bin/bash
#
# openstack-scripts/demos/mistral/hello/update.sh
#

source /etc/kolla/admin-openrc.sh

openstack workbook update \
  hello.wb.yaml

exit 0

#
