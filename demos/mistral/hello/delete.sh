#!/bin/bash
#
# openstack-scripts/demos/mistral/hello/delete.sh
#

source /etc/kolla/admin-openrc.sh

openstack workbook delete \
  hello

exit 0

#
