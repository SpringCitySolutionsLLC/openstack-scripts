#!/bin/bash
#
# openstack-scripts/demos/mistral/hello/create.sh
#

source /etc/kolla/admin-openrc.sh

openstack workbook create \
  --public \
  hello.wb.yaml

exit 0

#
