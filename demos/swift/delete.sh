#!/bin/bash
#
# openstack-scripts/demos/swift/delete.sh
#

source /etc/kolla/admin-openrc.sh

openstack container delete demo --recursive

exit 0

#
