#!/bin/bash
#
# openstack-scripts/demos/swift/show.sh
#

source /etc/kolla/admin-openrc.sh

echo openstack container show demo
openstack container show demo

echo openstack object list demo
openstack object list demo

echo openstack object show demo tmpfile.txt
openstack object show demo tmpfile.txt

echo openstack object save demo tmpfile.txt --file -
openstack object save demo tmpfile.txt --file -

exit 0

#
