#!/bin/bash
#
# openstack-scripts/demos/swift/create.sh
#

source /etc/kolla/admin-openrc.sh

date >> tmpfile.txt

echo Contents of tmpfile.txt
cat tmpfile.txt

openstack container create demo

openstack object create demo tmpfile.txt

rm tmpfile.txt

exit 0

#
