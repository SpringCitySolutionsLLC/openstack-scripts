#!/bin/bash
#
# openstack-scripts/demos/glance/cirros.sh
#

source /etc/kolla/admin-openrc.sh

echo This query answers the question, list all my cirros images sorted by age, newest first
echo

openstack image list \
  --property os_distro=cirros \
  -f yaml | grep Name | sort -nr

exit 0

#
