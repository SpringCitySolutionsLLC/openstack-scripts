#!/bin/bash
#
# openstack-scripts/demos/zun/pingtest/save-file.sh
#
# See also, read-file.sh

source /etc/kolla/admin-server-openrc.sh

mkdir -p transfer

rm -f transfer/*

date > transfer/savefile.txt

cat transfer/savefile.txt

# Note the behavior of zun cp transfer/savefile.txt pingtest:/root is identical

openstack appcontainer cp transfer/savefile.txt pingtest:/root

rm -f transfer/*

rmdir transfer

exit 0

#
