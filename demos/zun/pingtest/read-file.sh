#!/bin/bash
#
# openstack-scripts/demos/zun/pingtest/read-file.sh
#
# See also, save-file.sh

source /etc/kolla/admin-server-openrc.sh

mkdir -p transfer

rm -f transfer/*

# Note the behavior of zun cp pingtext:/root/savefile.txt transfer is identical

openstack appcontainer cp pingtest:/root/savefile.txt transfer

cat transfer/savefile.txt

rm -f transfer/*

rmdir transfer

exit 0

#
