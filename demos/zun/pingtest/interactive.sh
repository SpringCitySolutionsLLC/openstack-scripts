#!/bin/bash
#
# openstack-scripts/demos/zun/pingtest/interactive.sh
#

source /etc/kolla/admin-server-openrc.sh

zun exec --interactive pingtest /bin/sh

exit 0

#
