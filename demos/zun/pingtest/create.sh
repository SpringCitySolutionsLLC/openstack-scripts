#!/bin/bash
#
# openstack-scripts/demos/zun/pingtest/create.sh
#
# Note Pre-req: This demo uses the server-zun-net network

source /etc/kolla/admin-server-openrc.sh

zun create \
  --cpu 1 \
  --mem 1024 \
  --image-pull-policy always \
  --image-driver glance \
  --name pingtest \
  --net network=server-zun-net \
  cirros

exit 0

#
