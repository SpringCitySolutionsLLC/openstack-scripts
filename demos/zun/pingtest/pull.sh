#!/bin/bash
#
# openstack-scripts/demos/zun/pingtest/pull.sh
#

source /etc/kolla/admin-server-openrc.sh

docker pull cirros

openstack image delete cirros

docker save cirros | openstack image create cirros --public --container-format docker --disk-format raw

exit 0

#
