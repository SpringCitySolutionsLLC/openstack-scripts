#!/bin/bash
#
# openstack-scripts/demos/zun/pingtest/exec.sh
#

source /etc/kolla/admin-server-openrc.sh

zun exec pingtest ping -c4 10.10.1.1

exit 0

#
