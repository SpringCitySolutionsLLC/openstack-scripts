#!/bin/bash
#
# openstack-scripts/demos/trove/installcli.sh
#

# This is how I activate my kolla-ansible venv, just to make sure it's active when I install.
source ~/kolla-ansible/bin/activate

pip install python-troveclient -c https://releases.openstack.org/constraints/upper/yoga

exit 0

#
