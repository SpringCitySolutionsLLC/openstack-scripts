#!/bin/bash
#
# openstack-scripts/demos/heat/simple/create.sh
#

source /etc/kolla/admin-openrc.sh

export NET_ID=$(openstack network list | awk '/ prod-net / { print $2 }')

echo NET_ID is $NET_ID

openstack stack create --parameter "NetID=$NET_ID" -t simple.yml heat_demo_simple_stack

exit 0

#
