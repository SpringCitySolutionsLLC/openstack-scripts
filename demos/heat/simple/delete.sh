#!/bin/bash
#
# openstack-scripts/demos/heat/simple/delete.sh
#

source /etc/kolla/admin-openrc.sh

openstack stack delete -y heat_demo_simple_stack

exit 0

#
