#!/bin/bash
#
# openstack-scripts/demos/designate/zonelist.sh
#

dig NS enduser.os1.cedar.mulhollon.net. @os6
dig NS enduser.os2.cedar.mulhollon.net. @os6

dig NS enduser.os1.cedar.mulhollon.net. @dns11
dig NS enduser.os2.cedar.mulhollon.net. @dns11

dig NS enduser.os1.cedar.mulhollon.net. @dns12
dig NS enduser.os2.cedar.mulhollon.net. @dns12

exit 0

#
