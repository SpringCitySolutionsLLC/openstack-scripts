#!/bin/bash
#
# openstack-scripts/demos/designate/zonelist.sh
#

source /etc/kolla/admin-openrc.sh

openstack zone list --all-projects

exit 0

#
