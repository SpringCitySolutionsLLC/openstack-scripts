#!/bin/bash
#
# openstack-scripts/demos/cinder/quota-report.sh
#

date > quota-report.txt

echo >> quota-report.txt

echo ENDUSER >> quota-report.txt
openstack quota list -f yaml --project enduser --volume >> quota-report.txt

echo >> quota-report.txt

echo INFRASTRUCTURE >> quota-report.txt
openstack quota list -f yaml --project infrastructure --volume >> quota-report.txt

echo >> quota-report.txt

echo IOT >> quota-report.txt
openstack quota list -f yaml --project iot --volume >> quota-report.txt

echo >> quota-report.txt

echo RUTHERFORD >> quota-report.txt
openstack quota list -f yaml --project rutherford --volume >> quota-report.txt

echo >> quota-report.txt

echo SERVER >> quota-report.txt
openstack quota list -f yaml --project server --volume >> quota-report.txt

exit 0

#
