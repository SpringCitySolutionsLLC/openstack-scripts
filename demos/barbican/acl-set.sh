#!/bin/bash
#
# openstack-scripts/demos/barbican/acl-set.sh
#

source /etc/kolla/admin-openrc.sh

export SECRET_HREF=$(openstack secret list -f value --name message_for_julius | awk '{ print $1 }')

echo SECRET_HREF is $SECRET_HREF

# removes project-wide access and allows only secret creator and admin user to read
# could have done this with acl user add
openstack acl submit --no-project-access --user admin --operation-type read $SECRET_HREF

exit 0

#
