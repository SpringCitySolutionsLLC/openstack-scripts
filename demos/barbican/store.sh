#!/bin/bash
#
# openstack-scripts/demos/barbican/store.sh
#

source /etc/kolla/admin-openrc.sh

# Expires the day after the 2100th anniversary
openstack secret store --name message_for_julius --expiration 2056-03-16 --payload beware_the_ides_of_march

exit 0

#
