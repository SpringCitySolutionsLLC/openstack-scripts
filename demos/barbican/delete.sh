#!/bin/bash
#
# openstack-scripts/demos/barbican/delete.sh
#

source /etc/kolla/admin-openrc.sh

export SECRET_HREF=$(openstack secret list -f value --name message_for_julius | awk '{ print $1 }')

echo SECRET_HREF is $SECRET_HREF

openstack secret delete $SECRET_HREF

exit 0

#
