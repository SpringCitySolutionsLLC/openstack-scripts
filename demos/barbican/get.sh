#!/bin/bash
#
# openstack-scripts/demos/barbican/get.sh
#

source /etc/kolla/admin-openrc.sh

# Could have output YAML into a YAML parser
# Could have output JSON into a Python one-line script
# Sometimes the simplest way is an AWK script
export SECRET_HREF=$(openstack secret list -f value --name message_for_julius | awk '{ print $1 }')

echo The SECRET_HREF for name message_for_julius is $SECRET_HREF

openstack secret get $SECRET_HREF

openstack secret get --payload $SECRET_HREF

exit 0

#
