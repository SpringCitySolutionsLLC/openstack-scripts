#!/bin/bash
#
# openstack-scripts/demos/barbican/list.sh
#

source /etc/kolla/admin-openrc.sh

echo note the client can output in csv,json,table,value,yaml and here is an example of yaml
echo
# Note I'm filtering on the name message_for_julius to keep the output clean
# if there are other actually useful secrets being stored.
# Normally, you would leave the name option off to discover everything that is stored.
openstack secret list -f yaml --name message_for_julius

exit 0

#
