openstack coe cluster template create swarm-cluster-template \
                     --image fedora-atomic-latest \
                     --external-network provider1 \
                     --dns-nameserver 10.10.200.41 \
                     --master-flavor Small \
                     --flavor Small \
                     --coe swarm

