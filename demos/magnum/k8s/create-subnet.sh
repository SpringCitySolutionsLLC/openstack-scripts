openstack subnet create demo-magnum-subnet --network demo-magnum \
                                  --subnet-range 192.168.1.0/24 \
                                  --gateway 192.168.1.1 \
                                  --ip-version 4
