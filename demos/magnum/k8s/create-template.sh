openstack coe cluster template create kubernetes-cluster-template \
                     --image fedora-coreos-latest \
                     --external-network Provider1 \
                     --dns-nameserver 10.10.200.41 \
                     --master-flavor Small \
                     --flavor Small \
                     --coe kubernetes
