#!/bin/bash
#
# openstack-scripts/demos/nova/quota-report.sh
#

date > quota-report.txt

echo >> quota-report.txt

echo ENDUSER >> quota-report.txt
openstack quota list --project enduser --detail --compute >> quota-report.txt

echo >> quota-report.txt

echo INFRASTRUCTURE >> quota-report.txt
openstack quota list --project infrastructure --detail --compute >> quota-report.txt

echo >> quota-report.txt

echo IOT >> quota-report.txt
openstack quota list --project iot --detail --compute >> quota-report.txt

echo >> quota-report.txt

echo RUTHERFORD >> quota-report.txt
openstack quota list --project rutherford --detail --compute >> quota-report.txt

echo >> quota-report.txt

echo SERVER >> quota-report.txt
openstack quota list --project server --detail --compute >> quota-report.txt

exit 0

#
