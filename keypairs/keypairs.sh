#!/bin/bash
#
# openstack-scripts/keypairs/keypairs.sh
#

source /etc/kolla/admin-openrc.sh

openstack keypair create \
  --public-key ansible.pub \
  ansible

openstack keypair create \
  --public-key vince.pub \
  vince

exit 0
