README.md

# What is this?

These are PUBLIC scripts and config files and demonstrations I use for OpenStack that might be useful or inspirational or at least interesting for others to see.

This provides is consistent configuration across all my systems, at extreme speed, for low effort.

# Commentary

This repo makes the most sense in the context of being "downstream" of the following repos:

https://gitlab.com/SpringCitySolutionsLLC/glance-loader

and "upstream" of the following repos:

https://gitlab.com/SpringCitySolutionsLLC/ansible

https://gitlab.com/SpringCitySolutionsLLC/dhcp

## backups

These are the config files I use for OpenStack.

You might find something inspirational or interesting in them.

## demos

Simple demonstrations of various components of OpenStack.

## flavors

I like to manage my flavors via a simple CLI script.

There are slower, more difficult ways, but this way works for me.

## installcli

Automation to install the correct version of Python CLI libraries for this specific OpenStack Release Version

## keypairs

If you add my id_rsa.pub files to your cluster, then I can log into your instances.

Just kidding, that would be really dumb.

But I like managing my keypairs via a simple CLI script.

Maybe you would find that strategy useful?

## labelswift

This very small script applies labels to the NVME that are compatible with Swift container storage.

## networks

I manage my OpenStack provider networks via simple CLI shell scripts.

## projects

I manage my OpenStack projects and all containers and instances via OpenStack Heat Orchestration templates.

## ringmaker

Everyone has their own way to manage OpenStack Swift rings, this is my preferred way.

Have fun and play nice!
