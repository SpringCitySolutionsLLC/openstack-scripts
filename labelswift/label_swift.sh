#!/bin/bash

index=0
for d in nvme0n1; do
    parted /dev/${d} -s -- mklabel gpt mkpart KOLLA_SWIFT_DATA 1 -1
    sudo mkfs.xfs -f -L d${index} /dev/${d}p1
    (( index++ ))
done

